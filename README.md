# Athletes Test

- Developed with Vite, Vitest, React, TypeScript and Storybook

## Previews

- [Website](https://atheletes-test.pages.dev/)
- [Storybook](https://main--athletes-test-storybook.netlify.app/)

## Development

#### Install dependencies

```
  npm install
```

#### Build Website

```
  npm run build
```

#### Build Storybook

```
  npm run build-storybook
```

#### Local Development

```
  npm run dev
```

#### Local Storybook

```
  npm run storybook
```

#### Testing

```
  npm run test
```

#### Testing with coverage

```
  npm run test:coverage
```

#### Testing with ui

```
  npm run test:ui
```

#### Linting with fix

```
  npm run lint
```
