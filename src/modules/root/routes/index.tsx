import { RouteObject } from 'react-router-dom';

import landingRoutes from 'modules/landing/routes';

import ErrorPage from '../pages/ErrorPage';
import RootPage from '../pages/RootPage';

const rootRoutes = [
  {
    element: <RootPage />,
    errorElement: <ErrorPage />,
    children: [...landingRoutes],
  },
] as RouteObject[];

export default rootRoutes;
