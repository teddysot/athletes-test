import { RouteObject } from 'react-router-dom';

import LandingPage from '../pages/LandingPage';

const landingRoutes = [
  {
    path: '/',
    element: <LandingPage />,
  },
] as RouteObject[];

export default landingRoutes;
