export interface Athletes {
  id: string;
  title: string;
  description: string;
  descriptionColor: string;
  backgroundColor: string;
  underlineColor: string;
  titleIdColor: string;
}

export interface Players {
  id: string;
  title: string;
  description: string;
  descriptionColor: string;
  backgroundColor: string;
  underlineColor: string;
  titleIdColor: string;
}
