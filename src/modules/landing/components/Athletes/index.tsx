import { lazy, Suspense } from 'react';

import AmericanFootballDesktopImage from 'common/assets/american-football-desktop.svg';
import AmericanFootballMobileImage from 'common/assets/american-football-mobile.svg';
import AmericanFootballTabletImage from 'common/assets/american-football-tablet.svg';
import Informative from 'common/components/Informative';
import useDevice from 'common/hooks/useDevice';
import useResponsiveImage from 'common/hooks/useResponsiveImage';
import Header from 'modules/landing/components/Header';

import { ATHELETES_HEADER, ATHLETES_DATA } from './constants';
import { AmericanFootballStyled, Container } from './styles';

const Carousel = lazy(() => import('modules/landing/components/Carousel'));

const Athletes = () => {
  const { isMobile } = useDevice();
  const imageSrc = useResponsiveImage({
    mobile: AmericanFootballMobileImage,
    tablet: AmericanFootballTabletImage,
    desktop: AmericanFootballDesktopImage,
  });

  return (
    <Container>
      <AmericanFootballStyled
        src={imageSrc}
        alt="american-football-athletes-image"
        loading="lazy"
      />
      <Header title={ATHELETES_HEADER} position="right" />
      {!isMobile ? (
        <div>
          {ATHLETES_DATA.map((data) => (
            <Informative
              key={`${ATHELETES_HEADER}_${data.title}`}
              titleId={data.id}
              title={data.title}
              description={data.description}
              descriptionColor={data.descriptionColor}
              backgroundColor={data.backgroundColor}
              underlineColor={data.underlineColor}
              titleIdColor={data.titleIdColor}
              position="right"
            />
          ))}
        </div>
      ) : (
        <Suspense fallback={<div>Loading...</div>}>
          <Carousel header={ATHELETES_HEADER} data={ATHLETES_DATA} />
        </Suspense>
      )}
    </Container>
  );
};

export default Athletes;
