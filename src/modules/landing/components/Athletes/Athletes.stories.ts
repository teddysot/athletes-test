import Athletes from '.';

import type { Meta, StoryObj } from '@storybook/react';

const meta = {
  title: 'Landing/Athletes',
  component: Athletes,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
} satisfies Meta<typeof Athletes>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {},
};
