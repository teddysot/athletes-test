import { Athletes } from 'modules/landing/types';

export const ATHELETES_HEADER = 'ATHLETES';

export const ATHLETES_DATA = [
  {
    id: '01',
    title: 'CONNECTION',
    description:
      'Connect with coaches directly, you can ping coaches to view profile.',
    descriptionColor: 'black',
    backgroundColor: 'white',
    underlineColor: '#603ebe',
    titleIdColor: 'black',
  },
  {
    id: '02',
    title: 'COLLABORATION',
    description:
      'Work with other student athletes to increase visability. When you share and like other players videos it will increase your visability as a player. This is the team work aspect to Surface 1.',
    descriptionColor: 'black',
    backgroundColor: '#F5F4F9',
    underlineColor: '#603ebe',
    titleIdColor: 'black',
  },
  {
    id: '03',
    title: 'GROWTH',
    description:
      'Resources and tools for you to get better as a student Athelte. Access to training classes, tutor sessions, etc',
    descriptionColor: 'white',
    backgroundColor: '#5E3DB3',
    underlineColor: 'white',
    titleIdColor: '#8765DD',
  },
] as Athletes[];
