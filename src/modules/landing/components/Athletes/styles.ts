import styled from 'styled-components';

import { device } from 'common/constants/breakpoints';

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding-top: 100px;

  @media (max-width: ${device.tablet}) {
    padding-top: 50px;
    gap: 15px;
  }

  @media (max-width: ${device.mobile}) {
    padding-top: 20px;
    gap: 251px;
  }
`;

export const AmericanFootballStyled = styled.img`
  position: absolute;
  width: 100%;
  height: 50vw;
  max-width: 678px;
  max-height: 950px;
  top: 50px;
  left: 175px;
  z-index: 1;

  @media (max-width: ${device.tablet}) {
    top: 70px;
    left: -80px;
    width: 518px;
    height: 719px;
  }

  @media (max-width: ${device.mobile}) {
    max-width: 100%;
    top: 94px;
    left: 0px;
    width: 100%;
    height: 281px;
  }
`;
