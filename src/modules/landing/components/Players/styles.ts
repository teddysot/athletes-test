import styled from 'styled-components';

import { device } from 'common/constants/breakpoints';

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding-top: 100px;
  gap: 10px;

  @media (max-width: ${device.tablet}) {
    gap: 15px;
    padding-top: 50px;
  }

  @media (max-width: ${device.mobile}) {
    gap: 220px;
    padding-top: 20px;
  }
`;

export const BasketballImageStyled = styled.img`
  position: absolute;
  width: 100%;
  height: 50vw;
  max-width: 991px;
  max-height: 815px;
  z-index: 1;

  @media (max-width: ${device.desktop}) {
    top: 10px;
    left: 804px;
  }

  @media (max-width: ${device.tablet}) {
    top: 31px;
    left: unset;
    right: -127px;
    width: 691px;
    height: 568px;
  }

  @media (max-width: ${device.mobile}) {
    max-width: 100%;
    top: 94px;
    right: 0px;
    width: 100%;
    height: 250px;
  }
`;
