import { lazy, Suspense } from 'react';

import BasketballDesktopImage from 'common/assets/basketball-desktop.svg';
import BasketballMobileImage from 'common/assets/basketball-mobile.svg';
import BasketballTabletImage from 'common/assets/basketball-tablet.svg';
import Informative from 'common/components/Informative';
import useDevice from 'common/hooks/useDevice';
import useResponsiveImage from 'common/hooks/useResponsiveImage';
import Header from 'modules/landing/components/Header';

import { PLAYERS_DATA, PLAYERS_HEADER } from './constants';
import { BasketballImageStyled, Container } from './styles';

const Carousel = lazy(() => import('modules/landing/components/Carousel'));

const Players = () => {
  const { isMobile } = useDevice();
  const imageSrc = useResponsiveImage({
    mobile: BasketballMobileImage,
    tablet: BasketballTabletImage,
    desktop: BasketballDesktopImage,
  });

  return (
    <Container>
      <BasketballImageStyled
        src={imageSrc}
        alt="basketball-player-image"
        loading="lazy"
      />
      <Header title={PLAYERS_HEADER} position="left" />
      {!isMobile ? (
        <div>
          {PLAYERS_DATA.map((data) => (
            <Informative
              key={`${PLAYERS_HEADER}_${data.title}`}
              titleId={data.id}
              title={data.title}
              description={data.description}
              descriptionColor={data.descriptionColor}
              backgroundColor={data.backgroundColor}
              underlineColor={data.underlineColor}
              titleIdColor={data.titleIdColor}
              position="left"
            />
          ))}
        </div>
      ) : (
        <Suspense fallback={<div>Loading...</div>}>
          <Carousel header={PLAYERS_HEADER} data={PLAYERS_DATA} />
        </Suspense>
      )}
    </Container>
  );
};
export default Players;
