import Players from '.';

import type { Meta, StoryObj } from '@storybook/react';

const meta = {
  title: 'Landing/Players',
  component: Players,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
} satisfies Meta<typeof Players>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {};
