import { Players } from 'modules/landing/types';

export const PLAYERS_HEADER = 'PLAYERS';

export const PLAYERS_DATA = [
  {
    id: '01',
    title: 'CONNECTION',
    description:
      'Connect with talented athlete directly, you can watch their skills through video showreels directly from Surface 1.',
    descriptionColor: 'black',
    backgroundColor: 'white',
    underlineColor: '#603ebe',
    titleIdColor: 'black',
  },
  {
    id: '02',
    title: 'COLLABORATION',
    description:
      'Work with recruiter to increase your chances of finding talented athlete.',
    descriptionColor: 'black',
    backgroundColor: '#F5F4F9',
    underlineColor: '#603ebe',
    titleIdColor: 'black',
  },
  {
    id: '03',
    title: 'GROWTH',
    description: 'Save your time, recruit proper athlets for your team.',
    descriptionColor: 'white',
    backgroundColor: '#090C35',
    underlineColor: 'white',
    titleIdColor: '#8765DD',
  },
] as Players[];
