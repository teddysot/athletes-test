import { PLAYERS_DATA } from 'modules/landing/components/Players/constants';

import Carousel from '.';

import type { Meta, StoryObj } from '@storybook/react';

const meta = {
  title: 'Landing/Carousel',
  component: Carousel,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
} satisfies Meta<typeof Carousel>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    header: 'Carousel',
    data: PLAYERS_DATA,
  },
};
