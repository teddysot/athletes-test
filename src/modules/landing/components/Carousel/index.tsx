import { useEffect, useState } from 'react';

import Informative from 'common/components/Informative';
import { Athletes, Players } from 'modules/landing/types';

import { CarouselContainer } from './styles';

interface Props {
  header: string;
  data: Athletes[] | Players[];
}

const Carousel = ({ header, data }: Props) => {
  const [carouselIndex, setCarouselIndex] = useState(1);

  const carouselInfiniteScroll = () => {
    if (carouselIndex === data.length - 1) {
      return setCarouselIndex(0);
    }
    return setCarouselIndex(carouselIndex + 1);
  };

  useEffect(() => {
    const interval = setInterval(() => {
      carouselInfiniteScroll();
    }, 3000);

    return () => clearInterval(interval);
  });

  return (
    <CarouselContainer>
      <Informative
        titleId={data[carouselIndex].id}
        title={data[carouselIndex].title}
        description={data[carouselIndex].description}
        descriptionColor="black"
        backgroundColor="#F5F4F9"
        underlineColor="#603EBE"
        titleIdColor="#C2C2C2"
        header={header}
        carouselIndex={carouselIndex}
        onCarouselButtonClick={setCarouselIndex}
      />
    </CarouselContainer>
  );
};

export default Carousel;
