import { cleanup, render, screen } from '@testing-library/react';
import { describe, it, expect, afterEach } from 'vitest';

import '@testing-library/jest-dom/vitest';
import Header from '.';

describe('<Header />', () => {
  afterEach(() => {
    cleanup();
  });

  it('should render correctly', () => {
    render(<Header title="CONNECTION" position="right" />);

    expect(screen.getAllByText('CONNECTION')).toBeDefined();
  });
});
