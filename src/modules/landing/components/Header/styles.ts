import styled from 'styled-components';

import { device } from 'common/constants/breakpoints';

interface ContainerProps {
  $position: 'left' | 'right';
}

export const Container = styled.div<ContainerProps>`
  width: 50%;
  margin-left: ${({ $position }) => ($position === 'left' ? '321px' : '94px')};

  @media (max-width: ${device.tablet}) {
    width: 70%;
    margin-left: ${({ $position }) => ($position === 'left' ? '30px' : '84px')};
  }

  @media (max-width: ${device.mobile}) {
    width: 100%;
    margin-left: 20px;
  }
`;

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  height: 105px;

  @media (max-width: ${device.mobile}) {
    height: 59px;
    margin-top: 0px;
  }
`;

export const HeaderStyled = styled.div`
  font-size: 90px;
  line-height: 105.47px;
  color: #e7e7e7;
  width: 430px;
  height: 100%;

  @media (max-width: ${device.mobile}) {
    width: 239px;
    font-size: 50px;
    line-height: 58.59px;
    color: #e7e7e7;
  }
`;
