import EmptyContainer from 'common/components/EmptyContainer';
import useDevice from 'common/hooks/useDevice';

import { Container, HeaderStyled, Wrapper } from './styles';

interface Props {
  title: string;
  position: 'left' | 'right';
}

const Header = ({ title, position }: Props) => {
  const { isMobile } = useDevice();

  const isPosRight = !isMobile && position === 'right';
  const isPosLeft = !isMobile && position === 'left';

  return (
    <Wrapper>
      {isPosRight && <EmptyContainer />}
      <Container $position={position}>
        <HeaderStyled>{title}</HeaderStyled>
      </Container>
      {isPosLeft && <EmptyContainer />}
    </Wrapper>
  );
};

export default Header;
