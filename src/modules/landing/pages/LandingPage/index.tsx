import Athletes from 'modules/landing/components/Athletes';
import Players from 'modules/landing/components/Players';

import { LayoutContainer } from './styles';

const LandingPage = () => (
  <LayoutContainer>
    <Athletes />
    <Players />
  </LayoutContainer>
);

export default LandingPage;
