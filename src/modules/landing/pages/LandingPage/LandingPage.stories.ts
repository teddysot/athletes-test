import LandingPage from '.';

import type { Meta, StoryObj } from '@storybook/react';

const meta = {
  title: 'Landing/LandingPage',
  component: LandingPage,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
} satisfies Meta<typeof LandingPage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {},
};
