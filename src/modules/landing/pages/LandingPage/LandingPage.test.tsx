import { cleanup, render, screen } from '@testing-library/react';
import { describe, it, expect, afterEach } from 'vitest';

import '@testing-library/jest-dom/vitest';
import LandingPage from '.';

describe('<LandingPage />', async () => {
  vi.mock('modules/landing/components/Athletes', () => ({
    default: () => <div>AthletesMock</div>,
  }));

  vi.mock('modules/landing/components/Players', () => ({
    default: () => <div>PlayersMock</div>,
  }));

  afterEach(() => {
    cleanup();
  });

  it('should render correctly', () => {
    render(<LandingPage />);

    expect(screen.getAllByText('AthletesMock')).toBeDefined();
    expect(screen.getAllByText('PlayersMock')).toBeDefined();
  });
});
