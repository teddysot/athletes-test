interface Device {
  mobile: string;
  tablet: string;
  desktop: string;
}

export const device: Device = {
  mobile: '767px',
  tablet: '1529px',
  desktop: '4095px',
};
