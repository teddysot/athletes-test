import styled from 'styled-components';

import { device } from 'common/constants/breakpoints';

interface WrapperProps {
  $backgroundColor: string;
}

interface TitleIdProps {
  $fontColor: string;
}

interface DescriptionContainerProps {
  $descriptionColor: string;
  $position: string;
}

interface TitleIdUnderlineProps {
  $underlineColor: string;
}

interface DataContainerProps {
  $position: 'left' | 'right';
}

interface CarouselButtonProps {
  $isSelected: boolean;
}

export const Wrapper = styled.div<WrapperProps>`
  position: relative;
  background: ${({ $backgroundColor: backgroundColor }) => backgroundColor};
  display: flex;
  width: 100%;
  min-height: 211px;

  @media (max-width: ${device.tablet}) {
    min-height: 151px;
  }

  @media (max-width: ${device.mobile}) {
    flex-direction: column;
    min-height: 217px;
    max-height: 253px;
  }
`;

export const Container = styled.div`
  padding: 60px 0px 60px 0px;
  width: 56%;

  @media (max-width: ${device.tablet}) {
    width: 70%;
    padding: 30px 30px 30px 0px;
  }

  @media (max-width: ${device.mobile}) {
    padding: 60px 20px;
    width: 100%;
  }
`;

export const DataContainer = styled.div<DataContainerProps>`
  display: flex;
  flex-direction: column;
  gap: 20px;
  margin-left: ${({ $position }) => ($position === 'left' ? '322px' : '102px')};

  @media (max-width: ${device.tablet}) {
    gap: 20px;
    margin-left: ${({ $position }) => ($position === 'left' ? '30px' : '70px')};
  }

  @media (max-width: ${device.mobile}) {
    gap: 1px;
    margin-left: 0px;
  }
`;

export const TitleContainer = styled.div`
  display: flex;
  gap: 10px;
  align-items: center;

  @media (max-width: ${device.mobile}) {
    gap: 8px;
    margin-left: -2px;
  }
`;

export const TitleIdContainer = styled.div`
  width: 22px;
  height: 30px;

  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 4px;

  @media (max-width: ${device.mobile}) {
    gap: 1px;
  }
`;

export const Title = styled.div`
  width: 239px;
  height: 42px;
  line-height: 42.19px;
  letter-spacing: 1.5px;
  font-size: 36px;
  color: #c2c2c2;

  @media (max-width: ${device.mobile}) {
    font-size: 28px;
    line-height: 32.81px;
  }
`;

export const TitleId = styled.div<TitleIdProps>`
  font-size: 18px;
  line-height: 21.09px;
  letter-spacing: 1.5px;
  color: ${({ $fontColor }) => $fontColor};

  @media (max-width: ${device.mobile}) {
    font-size: 14px;
    line-height: 16.41px;
  }
`;

export const TitleIdUnderline = styled.div<TitleIdUnderlineProps>`
  width: 22px;
  height: 5px;
  border-radius: 5px;
  background: ${({ $underlineColor }) => $underlineColor};

  @media (max-width: ${device.mobile}) {
    width: 17.75px;
    height: 4px;
    border-radius: 4px;
  }
`;

export const DescriptionContainer = styled.div<DescriptionContainerProps>`
  color: ${({ $descriptionColor }) => $descriptionColor};
  font-size: 20px;
  line-height: 28px;
  max-width: 717px;

  @media (max-width: ${device.tablet}) {
    font-size: 18px;
    min-width: 447px;
    max-width: ${({ $position }) => ($position === 'left' ? '452px' : '630px')};
  }

  @media (max-width: ${device.mobile}) {
    font-size: 15px;
    line-height: 18.08px;
    min-width: 280px;
    max-width: calc(100% - 40px);
  }
`;

export const CarouselButton = styled.div<CarouselButtonProps>`
  cursor: pointer;
  border-radius: 10px;
  width: 10px;
  height: 10px;
  background-color: ${({ $isSelected }) =>
    $isSelected ? '#5C3CAF' : '#D8D8D8'};
`;

export const CarouselButtonContainer = styled.div`
  position: absolute;
  width: 100%;
  display: flex;
  gap: 12px;
  justify-content: center;
  bottom: 25px;
`;
