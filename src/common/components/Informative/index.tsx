import EmptyContainer from 'common/components/EmptyContainer';
import useDevice from 'common/hooks/useDevice';

import {
  CarouselButton,
  CarouselButtonContainer,
  Container,
  DataContainer,
  DescriptionContainer,
  Title,
  TitleContainer,
  TitleId,
  TitleIdContainer,
  TitleIdUnderline,
  Wrapper,
} from './styles';

interface Props {
  titleId: string;
  title: string;
  description: string;
  titleIdColor: string;
  backgroundColor: string;
  descriptionColor: string;
  underlineColor: string;
  header?: string;
  position?: 'left' | 'right';
  carouselIndex?: number;
  onCarouselButtonClick?: (index: number) => void;
}

const Informative = ({
  titleId,
  title,
  header,
  description,
  titleIdColor,
  backgroundColor,
  descriptionColor,
  underlineColor,
  position = 'right',
  carouselIndex,
  onCarouselButtonClick,
}: Props) => {
  const { isMobile } = useDevice();

  const isPosRight = !isMobile && position === 'right';
  const isPosLeft = !isMobile && position === 'left';

  const renderCarouselButtons = () => {
    if (!isMobile) return null;

    const buttons = [];
    for (let i = 0; i < 3; i++) {
      buttons.push(
        <CarouselButton
          key={`${header}_${title}_${i}_button`}
          $isSelected={carouselIndex === i}
          onClick={() => onCarouselButtonClick && onCarouselButtonClick(i)}
        />,
      );
    }

    return buttons;
  };

  return (
    <Wrapper $backgroundColor={backgroundColor}>
      {isPosRight && <EmptyContainer />}
      <Container>
        <DataContainer $position={position}>
          <TitleContainer>
            <TitleIdContainer>
              <TitleId $fontColor={titleIdColor}>{titleId}</TitleId>
              <TitleIdUnderline $underlineColor={underlineColor} />
            </TitleIdContainer>
            <Title>{title}</Title>
          </TitleContainer>
          <DescriptionContainer
            $position={position}
            $descriptionColor={descriptionColor}
          >
            {description}
          </DescriptionContainer>
        </DataContainer>
      </Container>
      {isPosLeft && <EmptyContainer />}
      {isMobile && (
        <CarouselButtonContainer>
          {renderCarouselButtons()}
        </CarouselButtonContainer>
      )}
    </Wrapper>
  );
};

export default Informative;
