import Informative from '.';

import type { Meta, StoryObj } from '@storybook/react';

const MOCK_DATA = {
  id: '01',
  title: 'CONNECTION',
  description:
    'Connect with talented athlete directly, you can watch their skills through video showreels directly from Surface 1.',
  descriptionColor: 'black',
  backgroundColor: 'white',
  underlineColor: '#603ebe',
  titleIdColor: 'black',
};

const meta = {
  title: 'Common/Informative',
  component: Informative,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
} satisfies Meta<typeof Informative>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    titleId: MOCK_DATA.id,
    title: MOCK_DATA.title,
    description: MOCK_DATA.description,
    descriptionColor: MOCK_DATA.descriptionColor,
    backgroundColor: MOCK_DATA.backgroundColor,
    underlineColor: MOCK_DATA.underlineColor,
    titleIdColor: MOCK_DATA.titleIdColor,
    position: 'left',
  },
};
