import styled from 'styled-components';

import { device } from 'common/constants/breakpoints';

export const Container = styled.div`
  width: 50%;

  @media (max-width: ${device.tablet}) {
    width: 30%;
  }

  @media (max-width: ${device.mobile}) {
    width: 80%;
  }
`;
