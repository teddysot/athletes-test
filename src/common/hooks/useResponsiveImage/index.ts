import useDevice from 'common/hooks/useDevice';

interface Props {
  mobile: string;
  tablet: string;
  desktop: string;
}

const useResponsiveImage = ({ mobile, tablet, desktop }: Props) => {
  const { isMobile, isTablet } = useDevice();
  if (isMobile) {
    return mobile;
  }

  if (isTablet) {
    return tablet;
  }

  return desktop;
};

export default useResponsiveImage;
