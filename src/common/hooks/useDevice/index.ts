import { useMediaQuery } from 'react-responsive';

import { device } from 'common/constants/breakpoints';

const useDevice = () => {
  const isMobile = useMediaQuery({
    query: `(max-width: ${device.mobile})`,
  });
  const isTablet = useMediaQuery({
    query: `(max-width: ${device.tablet})`,
  });
  const isDesktop = useMediaQuery({
    query: `(max-width: ${device.desktop})`,
  });

  return { isMobile, isTablet, isDesktop };
};

export default useDevice;
