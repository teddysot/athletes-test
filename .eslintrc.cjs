module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: [
    'airbnb',
    'airbnb-typescript',
    'prettier',
    'plugin:storybook/recommended',
  ],
  ignorePatterns: ['dist', '.eslintrc.cjs'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['tsconfig.json', 'tsconfig.node.json'],
  },
  plugins: ['prettier'],
  settings: {
    'import/resolver': {
      typescript: {
        project: 'tsconfig.json',
        alwaysTryTypes: true,
      },
      node: {
        paths: ['.'],
      },
      alias: {
        map: [['assets', './src/assets/*', 'components', './src/components/*']],
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
      },
    },
  },
  rules: {
    'no-plusplus': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/require-default-props': 'off',
    'react/function-component-definition': [
      'error',
      {
        namedComponents: 'arrow-function',
        unnamedComponents: 'arrow-function',
      },
    ],
    'prettier/prettier': ['error'],
    'import/no-extraneous-dependencies': 'off',
    'import/no-unresolved': 'off',
    'import/extensions': 'off',
    'import/prefer-default-export': 'off',
    'import/order': [
      1,
      {
        'newlines-between': 'always',
        groups: [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
          'object',
          'type',
        ],
        pathGroups: [
          { pattern: 'common/**', group: 'internal' },
          { pattern: 'modules/**', group: 'internal' },
        ],
        distinctGroup: false,
        pathGroupsExcludedImportTypes: ['internal'],
        alphabetize: { order: 'asc', caseInsensitive: true },
      },
    ],
  },
};
